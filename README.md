# Book List App Based on ReactjS and Redux

Interested in learning [Redux](https://www.udemy.com/react-redux/)?

### Getting Started

There are two methods for getting started with this repo.

#### Familiar with Git?
Checkout this repo, install dependencies, then start the gulp process with the following:

```
> git clone https://gitlab.com/omdjin/booklist.git
> cd booklist
> npm install
> npm start
```
